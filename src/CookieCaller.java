import java.io.IOException;
import java.net.*;

public class CookieCaller {

    SocketAddress address;
    DatagramSocket socket;

    public CookieCaller(InetAddress addr, int port){
        try {
            address = new InetSocketAddress(addr, port);
            socket = new DatagramSocket(address);
            //socket.joinGroup(address, NetworkInterface.getNetworkInterfaces().nextElement());
        } catch (SocketException e) {
            throw new RuntimeException(e);
        }
    }

    public void senden(){
        try {
            byte[] nachricht = new byte[100];
            DatagramPacket nachrichtenPacket = new DatagramPacket(nachricht, nachricht.length, address);
            socket.send(nachrichtenPacket);
            System.out.println("Nachricht " + nachricht + " wurde gesendet. Warte auf Antwort... ");

            byte[] antwort = new byte[100];
            DatagramPacket antwortPacket = new DatagramPacket(antwort, antwort.length, address);
            socket.receive(antwortPacket);
            System.out.println("Die Antwort war " + antwort);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public void empfangen(){
        while(true){
            try {
                byte[] anfrage = new byte[100];
                DatagramPacket anfragenPacket = new DatagramPacket(anfrage, anfrage.length, address);
                socket.receive(anfragenPacket);
                System.out.println("Anfrage erhalten: " + anfrage);

                byte[] bestaetigung = new byte[100];
                DatagramPacket bestaetigungPacket = new DatagramPacket(bestaetigung, bestaetigung.length, address);
                socket.send(bestaetigungPacket);
                System.out.println("Bestätigung gesendet");
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
    }
}
