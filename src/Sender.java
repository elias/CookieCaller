import java.io.IOException;
import java.net.*;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;

public class Sender{
    DatagramSocket sender;

    SocketAddress address;

    DatagramPacket nachrichtenPacket;
    byte[] nachricht;

    DatagramPacket antwortPacket;
    byte[] antwort;

    String absendername;

    public Sender(Inet4Address addr, int port, String name){
        address = new InetSocketAddress(addr, port);

        try {
            sender = new DatagramSocket(address);
            sender.setBroadcast(true);
            sender.setSoTimeout(5000);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        absendername = name;
        nachricht = new byte[156];
        nachrichtenPacket = null;

        antwort = new byte[16];
        antwortPacket = new DatagramPacket(antwort, antwort.length, address);
    }

    public void senden(String _nachricht){
        nachricht = bauePacket(absendername, _nachricht);
        nachrichtenPacket = new DatagramPacket(nachricht, nachricht.length, address);

        try {
            System.out.println("Nachricht '" + _nachricht + "' wird gesendet... ");
            sender.send(nachrichtenPacket);

            System.out.println("Nachricht gesendet. Warten auf Antwort...");

            do {
                sender.receive(antwortPacket);
            }while(antwort[0] == 0);

            System.out.println(getName(antwort) + " hat geantwortet");
            sender.close();
        } catch (IOException e) {
            System.out.println("Senden Fehlgeschlagen!");
            throw new RuntimeException(e);
        }
    }

    private byte[] bauePacket(String name, String nachricht){
        byte[] packet = new byte[160];

        packet[0] = 0;

        byte[] namenByte = name.getBytes(StandardCharsets.UTF_8);
        namenByte = Arrays.copyOf(namenByte, 15);
        for(int i = 1; i<= 15; i++){
            packet[i] = namenByte[i-1];
        }

        byte[] nachrichtenByte = nachricht.getBytes(StandardCharsets.UTF_8);
        nachrichtenByte = Arrays.copyOf(nachrichtenByte, 140);
        for(int i = 16; i<= 155; i++){
            packet[i] = nachrichtenByte[i-16];
        }

        return packet;
    }

    private String getName(byte[] _antwort){
        byte[] namenByte = new  byte[15];

        for(int i = 1; i <= 15; i++){
            namenByte[i-1] = _antwort[i];
        }

        String name = new String(namenByte, StandardCharsets.UTF_8);
        name = name.strip();

        return name;
    }

}