import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.UnknownHostException;

public class TestEmpfaenger {
    public static void main(String[] args){
        try {
            Empfaenger empfaenger = new Empfaenger((Inet4Address) InetAddress.getByName("0.0.0.0"), 56789, "Elias Wagner");
            empfaenger.empfangen();
        } catch (UnknownHostException e) {
            throw new RuntimeException(e);
        }
    }
}
