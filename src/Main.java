import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;

public class Main {
    public static void main(String[] args)  {
        System.out.println("Test Beginn");

        try {
            URL url = new URL("http://192.168.0.23/");
            String post_params = "anfrage=Test";

            HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();

            httpURLConnection.setRequestMethod("POST");
            httpURLConnection.setDoOutput(true);
            OutputStream os = httpURLConnection.getOutputStream();
            os.write(post_params.getBytes(StandardCharsets.UTF_8));
            os.flush();
            os.close();

            System.out.println(httpURLConnection.getResponseMessage());

            if (httpURLConnection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                System.out.println(new String(httpURLConnection.getInputStream().readAllBytes(), StandardCharsets.UTF_8));
            } else {
                System.out.println("Fehler bei der Verbindung");
            }

            httpURLConnection.disconnect();
        }catch (IOException exception){
            System.out.println("Fehler bei der Verbindung!");
        }

        System.out.println("Test Fertig");
    }
}