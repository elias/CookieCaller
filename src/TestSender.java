import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.UnknownHostException;

public class TestSender {
    public static void main(String[] args){
        System.setProperty("java.net.preferIPv4Stack", "true");

        try {
            Sender sender = new Sender((Inet4Address) InetAddress.getByName("192.168.0.255"), 56789, "Bernd Wagner");
            sender.senden("Jetzt aber wirklich! Ich schwörs euch wenn ihr jetzt nicht kommt gibts keine Burger. Ich hab für jeden einen, aber ich nehme auch gerne mehr als einen ihr dummen Wichser...");
        } catch (UnknownHostException e) {
            throw new RuntimeException(e);
        }
    }
}
